﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab5.Wyswietlacz.Kontrakt;
using System.Windows;
using System.ComponentModel;
using Lab5.DisplayForm;
using System.Xaml;

namespace Lab5.Wyswietlacz.Implementacja
{
    public class Wyswietlacz : IWyswietlacz
    {
        private DisplayViewModel model;

        public Wyswietlacz()
        {
            /* Uruchomienie kodu w wątku aplikacji okienkowej */
            model = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
            {
                // utworzenie nowej formatki graficznej stanowiącej widok
                var form = new Form();
                // utworzenie modelu widoku (wzorzec MVVM)
                var viewModel = new DisplayViewModel();
                // przypisanie modelu do widoku
                form.DataContext = viewModel;
                // wyświetlenie widoku
                form.Show();
                // zwrócenie modelu widoku do dalszych manipulacji
                return viewModel;
            }), null);
        }

        public void NowyNapis(string napis)
        {
            model.Text = napis;
        }
    }
}
