﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab5.Glowny.Kontrakt;
using Lab5.Wyswietlacz.Implementacja;
using Lab5.Wyswietlacz.Kontrakt;

namespace Lab5.Glowny.Implementacja
{
    public class Glowny : IGlowny
    {
        private IWyswietlacz wyswietlacz;

        public Glowny(IWyswietlacz wyswietlacz)
        {
            this.wyswietlacz = wyswietlacz;
        }

        public void WyswietlNowyNapis(string napis)
        {
            wyswietlacz.NowyNapis(napis);
        }
    }
}
