﻿using System;
using PK.Container;
using Lab5.MadContainer;
using Lab5.Glowny.Implementacja;
using Lab5.Glowny.Kontrakt;
using Lab5.Wyswietlacz.Implementacja;
using Lab5.Wyswietlacz.Kontrakt;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            IContainer MadContainer = new MadContainer.MadContainer();
            MadContainer.Register(typeof(Wyswietlacz.Implementacja.Wyswietlacz));
            MadContainer.Register(typeof(Glowny.Implementacja.Glowny));
            return MadContainer;
        }
    }
}
