﻿using System;
using System.Reflection;
using Lab5.MadContainer;
using Lab5.Glowny.Implementacja;
using Lab5.Glowny.Kontrakt;
using Lab5.Wyswietlacz.Implementacja;
using Lab5.Wyswietlacz.Kontrakt;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Lab5.MadContainer.MadContainer);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IGlowny));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Glowny.Implementacja.Glowny));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(IWyswietlacz));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Wyswietlacz.Implementacja.Wyswietlacz));

        #endregion
    }
}
