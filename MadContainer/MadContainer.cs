﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PK.Container;
using System.Reflection;

namespace Lab5.MadContainer
{
    public class MadContainer : IContainer
    {
        public Dictionary<Type, object> dictionaryOfInterfaceObject;
        public Dictionary<Type, Type> dictionaryOfInterfaceClass;

        public MadContainer()
        {
            this.dictionaryOfInterfaceObject = new Dictionary<Type, object>();
            this.dictionaryOfInterfaceClass = new Dictionary<Type, Type>();
        }

        public void Register(System.Reflection.Assembly assembly)
        {
            var types = assembly.GetTypes();
            foreach (var type in types)
            {
                if (type.IsPublic)
                {
                    this.Register(type);
                }
            }
        }

        public void Register(Type type)
        {
            if (type.IsClass)
            {
                var interfaces = type.GetInterfaces();
                foreach (var i in interfaces)
                {
                    if (!dictionaryOfInterfaceClass.ContainsKey(i))
                    {
                        dictionaryOfInterfaceClass[i] = type;
                    }
                    else
                    {
                        Console.WriteLine("This interafce is already registered!");
                    }
                }
            }
        }

        public void Register<T>(T impl) where T : class
        {
            var interfaces = impl.GetType().GetInterfaces();
            foreach (var i in interfaces)
            {
                if (!dictionaryOfInterfaceObject.ContainsKey(i))
                {
                    dictionaryOfInterfaceObject[i] = impl;
                }
            }
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            Register(provider.Invoke() as T);
        }

        public T Resolve<T>() where T : class
        {
            return (T)Resolve(typeof(T));
        }

        public object Resolve(Type type)
        {
            if (!dictionaryOfInterfaceClass.ContainsKey(type) && !dictionaryOfInterfaceObject.ContainsKey(type))
            {
                return null;
            }

            if (dictionaryOfInterfaceObject.ContainsKey(type))
            {
                return dictionaryOfInterfaceObject[type];
            }

            ConstructorInfo[] constructors = dictionaryOfInterfaceClass[type].GetConstructors();

            foreach (var constructor in constructors)
            {
                ParameterInfo[] parameters = constructor.GetParameters();
                if (parameters.Length == 0)
                {
                    return Activator.CreateInstance(dictionaryOfInterfaceClass[type]);
                }

                List<object> listOfParameters = new List<object>();

                foreach (var parameter in parameters)
                {
                    var a = Resolve(parameter.ParameterType);

                    if (a == null)
                    {
                        throw new UnresolvedDependenciesException();
                    }
                    listOfParameters.Add(a);
                }
                return Activator.CreateInstance(dictionaryOfInterfaceClass[type], listOfParameters.ToArray());
            }
            return null;
        }
    }
}
